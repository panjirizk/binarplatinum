# Binar Platinum Challenge

    Nama: Panji Rizky Pradana
    Wave 2 : DevOps Engineer (DOE)
    Fasilitator: Mochammad Syaifuddin Khasani

## Topologi
![App Screenshot](./images/topologi.png)


## Membuat Kubernetes Cluster di GCP
    1. klik Navigation Menu pada pojok kanan atas
    2. Pilih Kubernetes Engine
    3. Pilih Cluster lalu klik Create
    4. Pilih standard configure
    5. Lalu isi nama Cluster dan pilih Region sesuai kebutuhan
    6. Selanjutnya setting Node pool
    7. Aktifkan cluster dengan mencentang enable cluster auto scaler
    8. Enable VM Sport
    9. Terakhir klik create
![App Screenshot](./images/kubernetes cluster.png)

## Memasang ingress dengan Google Cloud Load Balancer memanfaatkan anotation
![App Screenshot](./images/ingress.png)

## Mengimplementasikan Domain + SSL untuk ingress
    1. Membuat IP Static pada staging dan production
![App Screenshot](./images/ip static.png)

    2. Tambahkan domain yang ada di ingress di bagian host:
![App Screenshot](./images/domain.png)

    3. Menambahkan SSL pada ingress menggunakan Google Cloud Provider di dalam file baru certificate.yml
![App Screenshot](./images/certificate.png)

    4. Setelah membuat config SSL maka kita harus memprbaharui file ingress.yml dengan menambahkan anotation
![App Screenshot](./images/ingressanotation.png)

## Membuat Gitlab Runner di Kubernetes Cluster
    1. Pastikan komputer sudah terinstall helm-chart
    2. Kunjungi artifacthub.io yang berisi config kubernetes manifest terhadap suatu platform yang sudah tersedia
    3. Pilih Gitlab-Runner yang sudah terverifikasi
    4. Copy isi Gitlab-runner manifest pada default values
    5. Buat file baru yang bernama value.yml untuk menyimpan manifest yang tadi di copy
    6. Pastikan gitlabUrl : https://gitlab.com/ pada no.51 dalam file value.yml
    7. Pastikan rbac: true pada no. 141 dan 142 dalam file value.yml
    8. Tambahkan tags: "kubernetes-runner" pada no.359 dalam file value.yml
    9. Pastikan Secret: gitlab-secret pada no. 393 dalam file value.yml
    10. Buat file secret-gitlab.yml dengan object secret untuk menyimpan register token runner dengan base64
![App Screenshot](./images/secretgitlab.png)

    11. Buat juga file namespace.yml untuk memisahkan project yang ada di kubernets cluster
![App Screenshot](./images/namespace.png)

    12. Terakhir menjalankan gitlab-runner dengan menggunakan helm-chart
![App Screenshot](./images/helmchart.png)
![App Screenshot](./images/runner.png)

## Mendeploy aplikasi Ke Kubernetes Cluster menggunakan Gitlab CI/CD Pipeline
    1. terdapat 2 stage yaitu build & deploy
    2. Setting Variable terlebih dahulu
![App Screenshot](./images/variable.png)

    3. Untuk melihat apakah CI/CD itu jalan maka kamu bisa lihat di bagian CI/CD > Pipelines
![App Screenshot](./images/cicd.png)

## Hasil Akhir
    Staging
![App Screenshot](./images/staging.png)

    Production
![App Screenshot](./images/production.png)